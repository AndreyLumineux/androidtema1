package com.example.app.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.app.R
import com.example.app.fragments.F1A2

class SecondActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        addF1A2()
    }

    private fun addF1A2()
    {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = F1A2::class.java.name
        val addTransaction = transaction.add(R.id.frame_layout, F1A2.newInstance(), tag)
        addTransaction.commit()
    }
}