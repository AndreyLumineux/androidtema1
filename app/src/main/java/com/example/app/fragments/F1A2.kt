package com.example.app.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.app.R

class F1A2 : Fragment()
{
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
                             ): View?
    {
        return inflater.inflate(R.layout.fragment_f1_a2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        val button: Button = view.findViewById(R.id.button)
        button.setOnClickListener { addf2a2() }
    }

    fun addf2a2()
    {

    }

    companion object
    {
        fun newInstance(): F1A2
        {
            val args = Bundle()
            val fragment = F1A2()
            fragment.arguments = args
            return fragment
        }
    }
}